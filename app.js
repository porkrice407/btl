var express = require('express');
var app = express();
var serv = require('http').Server(app);

app.get('/',function(req, res) {
	res.sendFile(__dirname + '/client/index.html');
});
app.use('/client',express.static(__dirname + '/client'));

serv.listen(2000);
console.log("Server started.");

var numOfCards = 60;

const Winner = {
  PLAYER1 : 'player1',
  PLAYER2 : 'player2',
  NONE : 'none'
}

const Combo_enum = {
  STRAIGHT_FLUSH: 'STRAIGHT_FLUSH',
  THREE_OF_A_KIND : 'THREE_OF_A_KIND',
  FLUSH: 'FLUSH',
  STRAIGHT: 'STRAIGHT',
  NONE : 'none'
}

cardMap = new Map()
cardMap.set("R1",{"text":"1","backgroundColor":"red"});
cardMap.set("R2",{"text":"2","backgroundColor":"red"});
cardMap.set("R3",{"text":"3","backgroundColor":"red"});
cardMap.set("R4",{"text":"4","backgroundColor":"red"});
cardMap.set("R5",{"text":"5","backgroundColor":"red"});
cardMap.set("R6",{"text":"6","backgroundColor":"red"});
cardMap.set("R7",{"text":"7","backgroundColor":"red"});
cardMap.set("R8",{"text":"8","backgroundColor":"red"});
cardMap.set("R9",{"text":"9","backgroundColor":"red"});
cardMap.set("R10",{"text":"10","backgroundColor":"red"});
cardMap.set("B1",{"text":"1","backgroundColor":"skyblue"});
cardMap.set("B2",{"text":"2","backgroundColor":"skyblue"});
cardMap.set("B3",{"text":"3","backgroundColor":"skyblue"});
cardMap.set("B4",{"text":"4","backgroundColor":"skyblue"});
cardMap.set("B5",{"text":"5","backgroundColor":"skyblue"});
cardMap.set("B6",{"text":"6","backgroundColor":"skyblue"});
cardMap.set("B7",{"text":"7","backgroundColor":"skyblue"});
cardMap.set("B8",{"text":"8","backgroundColor":"skyblue"});
cardMap.set("B9",{"text":"9","backgroundColor":"skyblue"});
cardMap.set("B10",{"text":"10","backgroundColor":"skyblue"});
cardMap.set("G1",{"text":"1","backgroundColor":"limegreen"});
cardMap.set("G2",{"text":"2","backgroundColor":"limegreen"});
cardMap.set("G3",{"text":"3","backgroundColor":"limegreen"});
cardMap.set("G4",{"text":"4","backgroundColor":"limegreen"});
cardMap.set("G5",{"text":"5","backgroundColor":"limegreen"});
cardMap.set("G6",{"text":"6","backgroundColor":"limegreen"});
cardMap.set("G7",{"text":"7","backgroundColor":"limegreen"});
cardMap.set("G8",{"text":"8","backgroundColor":"limegreen"});
cardMap.set("G9",{"text":"9","backgroundColor":"limegreen"});
cardMap.set("G10",{"text":"10","backgroundColor":"limegreen"});
cardMap.set("Y1",{"text":"1","backgroundColor":"yellow"});
cardMap.set("Y2",{"text":"2","backgroundColor":"yellow"});
cardMap.set("Y3",{"text":"3","backgroundColor":"yellow"});
cardMap.set("Y4",{"text":"4","backgroundColor":"yellow"});
cardMap.set("Y5",{"text":"5","backgroundColor":"yellow"});
cardMap.set("Y6",{"text":"6","backgroundColor":"yellow"});
cardMap.set("Y7",{"text":"7","backgroundColor":"yellow"});
cardMap.set("Y8",{"text":"8","backgroundColor":"yellow"});
cardMap.set("Y9",{"text":"9","backgroundColor":"yellow"});
cardMap.set("Y10",{"text":"10","backgroundColor":"yellow"});
cardMap.set("O1",{"text":"1","backgroundColor":"orange"});
cardMap.set("O2",{"text":"2","backgroundColor":"orange"});
cardMap.set("O3",{"text":"3","backgroundColor":"orange"});
cardMap.set("O4",{"text":"4","backgroundColor":"orange"});
cardMap.set("O5",{"text":"5","backgroundColor":"orange"});
cardMap.set("O6",{"text":"6","backgroundColor":"orange"});
cardMap.set("O7",{"text":"7","backgroundColor":"orange"});
cardMap.set("O8",{"text":"8","backgroundColor":"orange"});
cardMap.set("O9",{"text":"9","backgroundColor":"orange"});
cardMap.set("O10",{"text":"10","backgroundColor":"orange"});
cardMap.set("P1",{"text":"1","backgroundColor":"mediumpurple"});
cardMap.set("P2",{"text":"2","backgroundColor":"mediumpurple"});
cardMap.set("P3",{"text":"3","backgroundColor":"mediumpurple"});
cardMap.set("P4",{"text":"4","backgroundColor":"mediumpurple"});
cardMap.set("P5",{"text":"5","backgroundColor":"mediumpurple"});
cardMap.set("P6",{"text":"6","backgroundColor":"mediumpurple"});
cardMap.set("P7",{"text":"7","backgroundColor":"mediumpurple"});
cardMap.set("P8",{"text":"8","backgroundColor":"mediumpurple"});
cardMap.set("P9",{"text":"9","backgroundColor":"mediumpurple"});
cardMap.set("P10",{"text":"10","backgroundColor":"mediumpurple"});


var MAX_HAND = 3;

var Card = function(num, color) {
  var self = {
    card:color.text+num,
    num:num,
    color:color.text,
    backgroundColor:color.backgroundColor
  }
  self.toString = function() {
    return self.color + self.num;
  };
  return self;
}

var Hand = function(hand) {
  var self = {
    comboEnum:Combo_enum.NONE,
    combo:0, //lazy to figure out proper enum
    largestSum:0,
    largestNum:0,
    complete:hand.length == MAX_HAND
  }

  hand.sort(function(a, b){return a.num-b.num});
  var hand;

  var sameColor = true;
  var straight = true;
  var threeOfKind = true;
  self.largestSum = hand.length == 0 ? 0 : hand[0].num;
  self.largestNum = hand.length == 0 ? 0 : hand[hand.length - 1].num;

  for (var i = 1; i < hand.length; i++) {
    var prevCard = hand[i -1];
    var card  = hand[i];
    if (prevCard.color != card.color) {
      sameColor = false;
    }
    if (prevCard.num != card.num) {
      threeOfKind = false;
    }
    if (prevCard.num + 1 != card.num) {
      straight = false;
    }
    self.largestSum += card.num;
  }

  if (!hand.complete) {
    self.largestNum = self.largestNum + (MAX_HAND - hand.length);
  }
  var straightFlush = sameColor && straight;
  if (sameColor && straight) {
    self.comboEnum = Combo_enum.STRAIGHT_FLUSH;
    self.combo = 5;
  } else if (threeOfKind) {
    self.comboEnum = Combo_enum.THREE_OF_A_KIND;
    self.combo = 4;
  } else if (sameColor) {
    self.comboEnum = Combo_enum.FLUSH;
    self.combo = 3;
  } else if (straight) {
    self.combo = 2;
    self.comboEnum = Combo_enum.STRAIGHT;
  }
  return self;
}

var RuleEngine = function() {
  var self = {
  }
  self.findWinner = function(hand1, hand2) {
      var h1 = Hand(hand1);
      var h2 = Hand(hand2);
      if (!h1.complete && !h2.complete) {
        return Winner.NONE;
      }
      console.log("Player 1:" + h1.comboEnum);
      console.log("Player 2:" + h2.comboEnum);

      if (h1.complete && h2.complete) {
        return h1.combo > h2.combo ? Winner.PLAYER1 : Winner.PLAYER2;
      } else {
       // if straight flush
        if (h1.comboEnum == Combo_enum.STRAIGHT_FLUSH) {
          if (h1.complete && h2.complete) {
            return h1.largestNum > h2.largestNum ? Winner.PLAYER1 : Winner.PLAYER2;
          }
          console.log("Player 1 largestNum:" + h1.largestNum);
          console.log("Player 2 largestNum:" + h2.largestNum);
          if (h1.complete) {
            return h1.largestNum > h2.largestNum ? Winner.PLAYER1 : Winner.NONE;
          }
          return h2.largestNum > h1.largestNum ? Winner.PLAYER2 : Winner.NONE;
        }
        // if 3 of a kind
        if (h1.comboEnum == Combo_enum.THREE_OF_A_KIND) {
          if (h1.complete && h2.complete) {
            return h1.largestNum > h2.largestNum ? Winner.PLAYER1 : Winner.PLAYER2;
          }
          if (h1.complete) {
            return h1.largestNum > h2.largestNum ? Winner.PLAYER1 : Winner.NONE;
          }
          return h2.largestNum > h1.largestNum ? Winner.PLAYER2 : Winner.NONE;
        }

        // if flush
        if (h1.comboEnum == Combo_enum.FLUSH) {
           if (h1.complete && h2.complete) {
             return h1.largestSum > h2.largestSum ? Winner.PLAYER1 : Winner.PLAYER2;
           }
           //todo flush logic
           if (h1.complete) {
             return h1.largestSum > h2.largestSum ? Winner.PLAYER1 : Winner.NONE;
           }
           return h2.largestSum > h1.largestSum ? Winner.PLAYER2 : Winner.NONE;
        }

        // if straight
        if (h1.comboEnum == Combo_enum.STRAIGHT) {
          if (h1.complete && h2.complete) {
            return h1.largestNum > h2.largestNum ? Winner.PLAYER1 : Winner.PLAYER2;
          }
          if (h1.complete) {
            return h1.largestNum > h2.largestNum ? Winner.PLAYER1 : Winner.NONE;
          }
          return h2.largestNum > h1.largestPossibleNum ? Winner.PLAYER2 : Winner.NONE;
        }

        // if nothing
        if (h1.complete && h2.complete) {
          return h1.largestSum > h2.largestSum ? Winner.PLAYER1 : Winner.PLAYER2;
        }
        if (h1.complete) {
          return h1.largestSum > h2.largestSum ? Winner.PLAYER1 : Winner.NONE;
        }
        return h2.largestSum > h1.largestSum ? Winner.PLAYER2 : Winner.NONE;
      }
  }
  return self;
}

var Lane = function(id, player1Id, player2Id) {
  var self = {
    id: id,
    player1Id:player1Id,
    player2Id:player2Id,
    player1:[],
    player2:[],
    complete: false
  }
  self.canPlay = function(playerId) {
    return !self.complete && playerId == self.player1Id ? self.player1.length < MAX_HAND : self.player2.length < MAX_HAND;
  }
  self.placeCard = function(player, card) {
    var player = player.id == player1Id ? self.player1 : self.player2;
    player.push(card);
    console.log("placed cards in lane:" + player.toString());
    return player.length - 1;
  }
  self.display = function() {
    var display = "Lane " + self.id + " ";
    display += "Player 1: ";
    for(var i = 0; i < self.player1.length; i++) {
      display += self.player1[i].num + ",";
    }
    display += " Player 2: ";
    for(var i = 0; i < self.player2.length; i++) {
      display += self.player2[i].num + ",";
    }
    console.log(display);
  }
  self.findWinner = function() {
    if (self.player1.length == 3 && self.player2.length == 3) {
      var p1Sum = 0;
      self.player1.forEach(element => {
        p1Sum += element.num;
      });
      var p2Sum = 0;
      self.player2.forEach(element => {
        p2Sum += element.num;
      });
      console.log("Player 1 sum:" + p1Sum);
      console.log("Player 2 sum:" + p2Sum);
      return p1Sum > p2Sum ? Winner.PLAYER1 : Winner.PLAYER2;
    } else {
      return Winner.NONE;
    }
  }
  return self;
}

ruleEngine = RuleEngine();

//"unit testing"
//// non complete
//console.log(ruleEngine.findWinner(
//    [Card(1,'Y'),Card(2,'Y')],
//    [Card(7,'Y'),Card(6,'Y'), Card(8,'Y')]
//    )
//);
//
//console.log(ruleEngine.findWinner(
//    [Card(1,'Y')],
//    [Card(7,'Y'),Card(6,'Y'),Card(5,'Y')]
//    )
//);
//
//// complete
//console.log(ruleEngine.findWinner(
//    [Card(1,'Y'),Card(2,'Y'), Card(3,'Y')],
//    [Card(7,'Y'),Card(8,'Y'), Card(9,'Y')]
//    )
//);
//
//console.log(ruleEngine.findWinner(
//    [Card(2,'Y'),Card(7,'Y'), Card(3,'Y')],
//    [Card(7,'Y'),Card(6,'Y'), Card(5,'R')]
//    )
//);
//
//console.log(ruleEngine.findWinner(
//    [Card(1,'Y'),Card(4,'R'), Card(3,'Y')],
//    [Card(2,'G'),Card(8,'B'), Card(9,'Y')]
//    )
//);
//
//console.log(ruleEngine.findWinner(
//    [Card(1,'Y'),Card(1,'R'), Card(1,'Y')],
//    [Card(9,'G'),Card(9,'B'), Card(9,'Y')]
//    )
//);
//
//console.log(ruleEngine.findWinner(
//    [Card(2,'Y'),Card(7,'Y'), Card(3,'Y')],
//    [Card(9,'G'),Card(9,'B'), Card(9,'Y')]
//    )
//);

var Game = function() {
  var self = {
    deck:Deck(),
    player1:null,
    player2:null,
    started:false,
    currentPlayer:null,
    lanes:[]
  }

  self.placeCard = function(card, lane) {
//    console.log("player1:" + self.player1 + "Player:" + playerId + " play card:" + card.num + " on lane " + lane.id);
//    var player = playerId == self.player1.id ? self.player1 : self.player2;
    var player = context.currentPlayer;
    if (self.currentPlayer != player) {
      console.log("!not your turn");
      return {success:false, error:"Not your turn to play."};
    }
    if (self.lanes[lane].canPlay(player)) {
        var laneSlot = self.lanes[lane].placeCard(player, card);
        var index = player.playCard(card);
        var winner = ruleEngine.findWinner(self.lanes[lane].player1,self.lanes[lane].player2);
        if (winner != Winner.NONE) {
          self.lanes[lane].complete = true;
        }

        var winnerId = null;
        if (winner == Winner.PLAYER1) {
          winnerId = self.player1.id
        } else if (winner == Winner.PLAYER2) {
          winnerId = self.player2.id
        }
        player.draw(self.deck, index);
        self.switchPlayer();
        return {success:true, laneSlot: laneSlot, winnerId:winnerId};
    } else {
        return {success:false, error:"Cannot play on lane."};
    }
  }

  self.switchPlayer = function() {
    self.currentPlayer = self.currentPlayer == self.player1 ? self.player2 : self.player1;
  }

  self.addPlayer = function(playerId) {
    if (self.player1 && self.player2) {
      return {success:false};
    }
    if (!self.player1) {
       self.player1 = Player(playerId);
       console.log("palyer1" + self.player1)
       self.player1.initDraw(self.deck);
       self.currentPlayer = self.player1;
       return {success:true, player:self.player1};
    }
    self.player2 = Player(playerId);
    self.player2.initDraw(self.deck);
    self.startGame();
    return {success:true, player:self.player2};
  }

  self.startGame = function() {
    self.started = true;
    for(var i = 0 ; i < 9; i++){
        self.lanes.push(Lane(i, self.player1.id, self.player2.id));
    }
  }
  self.test = function() {
    self.player1.initDraw(self.deck);
      self.player2.initDraw(self.deck);

      for(var i = 0 ; i < 7; i++){
        console.log("Player 1: " + self.player1.hand[i].num);
        console.log("Player 2:" + self.player2.hand[i].num);
      }
      for(var i = 0 ; i < 9; i++){
        self.lanes[i].display();
      }
  }

  return self;
}

var Player = function(id) {
  var self = {
    id:id,
    hand:[]
  }
  self.toString = function() {
    var handString = self.hand.toString();
    return "playerid:" + self.id + " hand:" + handString;
  }
  self.draw = function(deck, index = 0) {
    console.log("before draw" + self.hand.toString());
    self.hand.splice(index, 0, deck.deal());
    console.log("after draw" + self.hand.toString());
  }
  self.initDraw = function(deck) {
    for(var i = 0 ; i < 7; i++){
      self.draw(deck);
    }
  }
  self.playCard = function(card){
    for (var i = 0; i < self.hand.length; i++) {
      if (self.hand[i].card == card) {
        self.hand.splice(i, 1)
        return i;
      }
    }
    throw "play card incorrect";
  }
  return self;
}

colors = [
    {text:"R", backgroundColor:"red"},
    {text:"B", backgroundColor:"skyblue"},
    {text:"G", backgroundColor:"limegreen"},
    {text:"Y", backgroundColor:"yellow"},
    {text:"O", backgroundColor:"orange"},
    {text:"P", backgroundColor:"mediumpurple"}];

var Deck = function() {
  var self = {
    cards:[]
  }
  var shuffle = function (array) {

  	var currentIndex = array.length;
  	var temporaryValue, randomIndex;

  	// While there remain elements to shuffle...
  	while (0 !== currentIndex) {
  		// Pick a remaining element...
  		randomIndex = Math.floor(Math.random() * currentIndex);
  		currentIndex -= 1;

  		// And swap it with the current element.
  		temporaryValue = array[currentIndex];
  		array[currentIndex] = array[randomIndex];
  		array[randomIndex] = temporaryValue;
  	}

  	return array;

  };
  self.init = function() {
    for (var i = 0; i < colors.length; i++) {
        for (var j = 1; j <= 10; j++) {
          console.log('cardMap.set("'+colors[i].text+j+'",{"text":"'+j+'","backgroundColor":"'+colors[i].backgroundColor+'"});');self.cards.push(Card(j, colors[i]));
        }
    }
    shuffle(self.cards);
  }
  self.deal = function() {
    return self.cards.pop();
  }
  self.init();
  return self;
}

var Context = function(playerId, game) {
  var player = playerId == game.player1.id ? game.player1 : game.player2;
  var self = {
    currentPlayer:player
  }
  return self;
}
game = Game();
context = null;
var io = require('socket.io')(serv,{});
io.sockets.on('connection', function(socket){
	console.log('socket connection');

  socket.on('submit',function(data){
  		context = Context(data.playerId, game)
      console.log('Submitting Player:' + context.currentPlayer + 'Card:' + data.card + " Lane:" +
       data.lane);
      console.log('playing card' + data.card);
  		var result = game.placeCard(data.card, data.lane);
      if (result.success) {
        if (result.winnerId) {
          io.sockets.emit('laneWinner', {
            lane:data.lane,
            winnerId:result.winnerId
          })
        }
        socket.emit('renderLane', {
            lane:data.lane,
            laneSlot:result.laneSlot,
            card:cardMap.get(data.card)
        })
        socket.broadcast.emit('renderOpponentLane', {
            lane:data.lane,
            laneSlot:result.laneSlot,
            card:cardMap.get(data.card)
        })
        console.log("Player:" + context.currentPlayer.toString());
        socket.emit('initPlayer',{
          hand:context.currentPlayer.hand,
        });
      } else {
          console.log("found errror" + result.error);
         socket.emit('exception', {
                message:result.error
          });
      }

  });

  socket.on('joinGame',function(data){
        var result = game.addPlayer(data.playerId);
        context = Context(data.playerId, game)
        console.log("Player:" + context.currentPlayer.toString());
        if (result.success) {
             socket.emit('initPlayer',{
                  playerId:result.player.id,
              		hand:result.player.hand
            });
            if (game.started) {
              io.sockets.emit('gameStarted',{
                message:"Game Started."
              });
            }
        } else {
            socket.emit('exception',{
              message:"No more room for new player.",
            });
        }

  });
  });